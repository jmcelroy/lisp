#!/usr/bin/clisp

(setf thing "hello world")
(pprint thing)
(pprint (string-upcase thing))

;;; random number dice roller
;;; https://opensource.com/article/21/5/learn-lisp

(defun roller (number)
    (pprint (random (parse-integer (nth 0 number))))
)

(setf userput *args*)
(setf *random-state* (make-random-state t))
(roller userput)